﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Pregunta : MonoBehaviour
{
    public GameObject pregunta, A, B, C, D, Timer;
    public int dificultad;
    public float time;
    public bool inicio, cierrame;
    public int respuesta, respuestaCorrecta;
    public int correcto;
    public bool snadder;
    public SQLPregunta sqlpregunta;
    public int tema;

    public void Iniciar(bool snader)
    {
        Debug.Log("funcion: INICIAR");
        dificultad = Random.Range(1,3);
        tema = FlechaRuleta.tema;
        time = GetTime();
        correcto = 0;
        inicio = true;
        cierrame = false;
        respuesta = 0;
        this.snadder = snader;

        //Buscamos nuestra pregunta
        sqlpregunta = new SQLPregunta(tema, dificultad);
        pregunta.GetComponent<TextMeshProUGUI>().text = sqlpregunta.pregunta;
        A.GetComponentInChildren<TextMeshProUGUI>().text = sqlpregunta.aaa;
        B.GetComponentInChildren<TextMeshProUGUI>().text = sqlpregunta.bbb;
        C.GetComponentInChildren<TextMeshProUGUI>().text = sqlpregunta.ccc;
        D.GetComponentInChildren<TextMeshProUGUI>().text = sqlpregunta.ddd;
        respuestaCorrecta = sqlpregunta.DameRespuestaCorrectaInt();

        if (!sqlpregunta.existe)
        {
            B.GetComponent<ButtonDelay>().Lock();
            C.GetComponent<ButtonDelay>().Lock();
            D.GetComponent<ButtonDelay>().Lock();
        }
    }

    public void Cerrar()
    {
        Debug.Log("funcion: CERRAR");
        correcto = 0;
        inicio = false;
        cierrame = true;
        respuesta = 0;
        respuestaCorrecta = 0;

        B.GetComponent<ButtonDelay>().UnLock();
        C.GetComponent<ButtonDelay>().UnLock();
        D.GetComponent<ButtonDelay>().UnLock();
    }

    // Update is called once per frame
    void Update ()
    {
        if (!inicio)
        {
            return;
        }

        time -= Time.deltaTime;
        string snakeorladder;
        if (snadder)
        {
            snakeorladder = "Para subir escalera \n";
        }
        else
        {
            snakeorladder = "Para no bajar serpiente \n";
        }
        Timer.GetComponent<TextMeshProUGUI>().SetText(snakeorladder+"Secs: " + Mathf.Floor(time));
        if (time <= 0 || correcto < 0)
        {
            //Deberiamos de mostrar una animacion de que esta bien o mal
            Debug.Log("SEÑAL: CIERRAME");
            cierrame = true;
            correcto = -1;
        }

        if (respuesta != 0)
        {
            if (respuesta == respuestaCorrecta)
            {
                Debug.Log("Correcto: Pusiste: "+respuesta);
                correcto = 1;
            }
            else
            {
                Debug.Log("Incorrecto: Pusiste: " + respuesta);
                correcto = -1;
            }

            cierrame = true;
        }
	}

    int GetTime()
    {
        return 60*dificultad;
    }

    public void Respuesta(int res)
    {
        this.respuesta = res;
    }
}
