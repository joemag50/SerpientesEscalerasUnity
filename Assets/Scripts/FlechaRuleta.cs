﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlechaRuleta : MonoBehaviour
{
	GameObject Ruleta;
	public static string elementoObtenido;
	public static bool obtenido;
	public List<string> elementos;
    public static int tema;

	void Start()
	{
		Ruleta = GameObject.FindGameObjectWithTag("Ruleta");
		obtenido = false;
		elementos = new List<string>();
	}

	void OnTriggerStay(Collider col)
	{
		int clicks = Ruleta.GetComponent<SpinDaWheel>().clicks;
		if (Ruleta.GetComponent<SpinDaWheel>().detenida &&
			clicks > 0 &&
			elementos.Count < clicks)
		{
			Debug.Log(col.gameObject.name);
			elementoObtenido = col.gameObject.name;
            tema = int.Parse(elementoObtenido.Split(':')[0]);
			obtenido = true;
			elementos.Add(col.gameObject.name);
		}
	}
}
