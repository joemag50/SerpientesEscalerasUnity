﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador : MonoBehaviour
{
    public int lugar;
    public bool MeEstoyMoviendo;
    public List<Mosaico> mosaicos;
    public bool abrePregunta;
    public int tamanoSnadder;
    public bool EstaEnUltimaCasilla;

    // Use this for initialization
    void Start ()
    {
        this.lugar = 0;
        this.MeEstoyMoviendo = false;
        this.abrePregunta = false;
        this.tamanoSnadder = 0;
        this.EstaEnUltimaCasilla = false;
    }

    // Update is called once per frame
    void Update ()
    {
        
	}

    public void misMosaicos(List<Mosaico> mos)
    {
        this.mosaicos = mos;
    }

    public void Mover(int cuantos)
    {
        this.MeEstoyMoviendo = true;
        if (cuantos == 0)
            return;

        this.lugar += cuantos;
        if (this.lugar < this.mosaicos.Count - 1)
        {
            GameObject mos = this.mosaicos[this.lugar].mosaico;
            this.transform.position = mos.transform.position;

            if (this.mosaicos[this.lugar].snadder != 0)
            {
                //Debug.Log("Detente ahi cabron!");
                this.abrePregunta = true;
                this.tamanoSnadder = this.mosaicos[this.lugar].snadder;
            }
        }
        else
        {
            GameObject mos = this.mosaicos[this.mosaicos.Count - 1].mosaico;
            this.transform.position = mos.transform.position;

            //INICIAR PREGUNTA MATONA
            this.EstaEnUltimaCasilla = true;
            this.abrePregunta = true;
            //Debug.Log("JUEGO TERMINADO");
        }
    }

    public void MoverEnSnadder()
    {
        this.lugar += this.mosaicos[this.lugar].snadder;
        GameObject mos = this.mosaicos[this.lugar].mosaico;
        this.transform.position = mos.transform.position;
    }
}
