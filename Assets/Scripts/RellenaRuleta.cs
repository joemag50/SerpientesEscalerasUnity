﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using System.IO;
using System.Text;
using System.Data;
using Mono.Data.SqliteClient;

public class RellenaRuleta : MonoBehaviour
{
    public GameObject Ruleta;
    public GameObject labelTema;
    public List<string> temas;

    private GameObject[] Elementos;
    private bool completado;

    // Use this for initialization
    void Start ()
    {
        completado = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (GetComponent<AjustaLasBolas>().BolasAjustadas && !completado)
        {
            dameTemas();
            colocaLosCanvas();
            completado = true;
        }
	}

    public void colocaLosCanvas()
    {
        Elementos = GameObject.FindGameObjectsWithTag("BolaPedorra");
        int i = 0;
        Vector3 rot = new Vector3(0,0,-90f + 9.23f);
        foreach (GameObject go in Elementos)
        {
            GameObject m;
            m = Instantiate(labelTema, Ruleta.transform.position + new Vector3(0,0,-1f), Quaternion.identity);
            m.name = "Canv-" + i;
            go.name = i + ": " + this.temas[i];
            m.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0f);
            m.transform.SetParent(go.transform);
            m.GetComponentInChildren<TextMeshProUGUI>().text = this.temas[i];
            m.GetComponentInChildren<TextMeshProUGUI>().fontSize = 200f;
            m.GetComponentsInChildren<RectTransform>()[1].sizeDelta = new Vector2(3300f, 250f);
            m.transform.localScale = new Vector3(1,1,1);
            m.transform.rotation = Quaternion.Euler(rot);
            m.GetComponent<RectTransform>().sizeDelta = new Vector2(25f, 350f);
            i++;
            rot.z += 9.23f; //39 / 360
        }
        Debug.Log(i);
    }

    public void dameTemas()
    {
        this.temas = new List<string>();
        IDataReader reader = BaseDatos.SqlCommand("SELECT * FROM temas");
        while (reader.Read())
        {
            this.temas.Add(reader.GetString(1));
        }
    }
}
