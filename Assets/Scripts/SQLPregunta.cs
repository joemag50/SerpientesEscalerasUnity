﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Data;
using Mono.Data.SqliteClient;

public class SQLPregunta
{
    public int idpregunta;
    public int idtema;
    public int idpregunta_tema;
    public string pregunta;
    public string aaa, bbb, ccc, ddd;
    public string correcta;
    public bool existe;

    public SQLPregunta(int idtema, int idpregunta_tema)
    {
        this.existe = false; //Vamos a decir que no existe
        this.idtema = idtema;
        this.idpregunta_tema = idpregunta_tema;
        this.BuscaPregunta();

        if (!this.existe)
        {
            this.pregunta = "Esta es una pregunta comodín \n Presiona el botón 1";
            this.aaa = "¡Presioname!";
            this.bbb = "";
            this.ccc = "";
            this.ddd = "";
            this.correcta = "a";
        }
    }

    public void BuscaPregunta()
    {
        IDataReader reader = BaseDatos.SqlCommand(string.Format("SELECT * FROM preguntas WHERE idtema = '{0}' AND idpregunta_tema = '{1}' ", this.idtema, this.idpregunta_tema));
        if (reader.Read())
        {
            this.existe = true; //Lo cambiamos si existe
            this.idpregunta = reader.GetInt32(0);
            this.pregunta = reader.GetString(3);
            this.aaa = reader.GetString(4);
            this.bbb = reader.GetString(5);
            this.ccc = reader.GetString(6);
            this.ddd = reader.GetString(7);
            this.correcta = reader.GetString(8);
        }
    }

    public int DameRespuestaCorrectaInt()
    {
        switch (this.correcta)
        {
            case "a":
                return 1;
            case "b":
                return 2;
            case "c":
                return 3;
            case "d":
                return 4;
            default:
                break;
        }
        return 0;
    }
}
