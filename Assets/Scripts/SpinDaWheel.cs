﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinDaWheel : MonoBehaviour
{
	public bool detener;
	public bool detenida;
	float time ;
	public int clicks;
	// Use this for initialization
	void Start ()
	{
		detener = false;
		detenida = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (detener)
		{
			time -= Time.deltaTime;
			if (time < 0)
			{
				//Detener ruleta
				GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
				detener = false;
				detenida = true;
			}
		}
	}

	public void GiraLaRuleta()
	{
		GetComponent<Rigidbody>().AddTorque(new Vector3(0,0,1000));
		detener = true;
		detenida = false;
		time = Random.Range(2, 9);
		clicks++;
	}
}
