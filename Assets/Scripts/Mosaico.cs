﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mosaico
{
    public GameObject mosaico;
    public int snadder;
    public int index;

    public Mosaico(GameObject mosaico, int index, int snadder)
    {
        this.mosaico = mosaico;
        this.snadder = snadder;
        this.index = index;
    }

    public Vector3 GetCenter()
    {
        return mosaico.transform.position + new Vector3(0,5,0); ;
    }

    public void SetSnadder(int snadder)
    {
        this.snadder = snadder;
    }
}
