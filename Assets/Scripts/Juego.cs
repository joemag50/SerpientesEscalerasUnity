﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Juego : MonoBehaviour
{
    public GameObject Jugador;
    public GameObject CanvasPregunta;
    public GameObject CanvasMensajes;
    public GameObject Roll;
    public int PosActualJugadores;
    public int PosNuevaJugadores;
    public int ValorRuleta;
    public bool RuletaActiva;
    public bool CerrarJuego;
    public float TiempoCambiarCamara; //El tiempo para cambiar de camara despues de obtener el valor de la ruleta
    public float TiempoAntesMostrarRuleta; //El tiempo antes de cambiarnos a la camara de la ruleta, espero nos deje para mostrar un mensaje
    public float TiempoParaCerrarJuego;
    public float TiempoParaHabilitarBoton;
    // Use this for initialization
    void Start()
    {
        this.Jugador = GameObject.FindGameObjectWithTag("Player");

        PosActualJugadores = 0;
        PosNuevaJugadores = 0;
        ValorRuleta = 0;
        RuletaActiva = false;
        TiempoCambiarCamara = 4;
        TiempoAntesMostrarRuleta = 4;
        TiempoParaCerrarJuego = 4;
        TiempoParaHabilitarBoton = 3;
        CerrarJuego = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!DiceScript.rolling)
        {
            if (!(PosActualJugadores + DiceScript.diceNumber == PosNuevaJugadores) && DiceScript.diceNumber > 0 && !DiceScript.rolling)
            {
                this.Jugador.GetComponent<Jugador>().Mover(DiceScript.diceNumber);
            }

            PosNuevaJugadores = DiceScript.diceNumber + this.Jugador.GetComponent<Jugador>().lugar;
            PosActualJugadores = this.Jugador.GetComponent<Jugador>().lugar;
        }

        if (this.Jugador.GetComponent<Jugador>().abrePregunta)
        {
            TiempoAntesMostrarRuleta -= Time.deltaTime;
            this.CanvasMensajes.GetComponent<ActivarPanel>().MuestraPanel("ruleta");
            if (TiempoAntesMostrarRuleta < 0)
            {
                //Debug.Log("Señal: Muestra Ruleta");
                this.MostrarRuleta();
                this.Jugador.GetComponent<Jugador>().abrePregunta = false;
                TiempoAntesMostrarRuleta = 4;
            }
        }

        if (this.CanvasPregunta != null)
        {
            if (this.Jugador.GetComponent<Jugador>().EstaEnUltimaCasilla)
            {
                if (this.CanvasPregunta.GetComponent<Pregunta>().correcto > 0) //Esta bien la respuesta
                {
                    //Ganaste
                    this.CanvasMensajes.GetComponent<ActivarPanel>().MuestraPanel("ganaste");
                }
                else if (this.CanvasPregunta.GetComponent<Pregunta>().correcto < 0) //Esta mal la respuesta
                {
                    //Perdiste
                    this.CanvasMensajes.GetComponent<ActivarPanel>().MuestraPanel("perdiste");
                }

                //Significa que este bien o mal, empezamos a contar, pero no cuando esta en standby
                if (this.CanvasPregunta.GetComponent<Pregunta>().correcto != 0)
                {
                    //Terminar Juego
                    CerrarJuego = true;
                }
            }

            if (this.CanvasPregunta.GetComponent<Pregunta>().correcto > 0 && this.Jugador.GetComponent<Jugador>().tamanoSnadder > 0)
            {
                this.Jugador.GetComponent<Jugador>().MoverEnSnadder();
            }
            else if (this.CanvasPregunta.GetComponent<Pregunta>().correcto < 0 && this.Jugador.GetComponent<Jugador>().tamanoSnadder < 0)
            {
                this.Jugador.GetComponent<Jugador>().MoverEnSnadder();
            }

            if (this.CanvasPregunta.GetComponent<Pregunta>().correcto > 0)
            {
                this.CanvasMensajes.GetComponent<ActivarPanel>().MuestraPanel("correcto");
            }
            else if (this.CanvasPregunta.GetComponent<Pregunta>().correcto < 0)
            {
                this.CanvasMensajes.GetComponent<ActivarPanel>().MuestraPanel("incorrecto");
            }

            if (this.CanvasPregunta.GetComponent<Pregunta>().cierrame)
            {
                this.BorrarPregunta();
            }
        }

        if (CerrarJuego)
        {
            TiempoParaCerrarJuego -= Time.deltaTime;
            //Redireccionar Menu Principal
            if (TiempoParaCerrarJuego < 0)
            {
                TerminarPartida();
            }
        }

        if (RuletaActiva)
        {
            if (FlechaRuleta.obtenido)
            {
                TiempoCambiarCamara -= Time.deltaTime;
                if (TiempoCambiarCamara < 0)
                {
                    this.MostrarRuleta();
                    this.NuevaPregunta();
                    TiempoCambiarCamara = 4;
                    FlechaRuleta.obtenido = false;
                }
            }
        }
    }

    public void MostrarRuleta()
    {
        CambiarCamara.CualCamara = 1;
        RuletaActiva = !RuletaActiva;
    }

    public void NuevaPregunta()
    {
        //Debug.Log("funcion: Abre pregunta");
        this.CanvasPregunta.SetActive(true);
        bool snadder = (this.Jugador.GetComponent<Jugador>().tamanoSnadder >= 0);
        this.CanvasPregunta.GetComponent<Pregunta>().Iniciar(snadder);
    }

    public void BorrarPregunta()
    {
        this.CanvasPregunta.SetActive(false);
        this.CanvasPregunta.GetComponent<Pregunta>().Cerrar();
    }

    public void TerminarPartida()
    {
        SceneManager.LoadScene("MenuPrincipal");
    }

    public void ReiniciarPartida()
    {
        SceneManager.LoadScene("Juego");
    }
}
