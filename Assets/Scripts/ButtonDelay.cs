﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonDelay : MonoBehaviour
{
    public bool presionado;
    public bool locked;
    public float timeDel;

	// Use this for initialization
	void Start ()
    {
        timeDel = 3;
        locked = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (presionado)
        {
            timeDel -= Time.deltaTime;
            if (timeDel < 0)
            {
                timeDel = 3;
                presionado = false;
                GetComponent<Button>().interactable = true;
            }
            return;
        }

        if (locked)
        {
            GetComponent<Button>().interactable = false;
            return;
        }
        else
        {
            GetComponent<Button>().interactable = true;
        }
    }

    public void Delay()
    {
        presionado = true;
        GetComponent<Button>().interactable = false;
    }

    public void Lock()
    {
        locked = true;
    }

    public void UnLock()
    {
        locked = false;
    }
}
