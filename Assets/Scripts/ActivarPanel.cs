﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarPanel : MonoBehaviour
{
    public GameObject PHoraDeLaRuleta, PGanaste, PPerdiste, PCorrecto, PIncorrecto;
    private bool Activo;
    private float tiempo;
	// Use this for initialization
	void Start ()
    {
        this.Activo = false;
        this.tiempo = 4f;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (this.Activo)
        {
            this.tiempo -= Time.deltaTime;
            //Debug.Log(this.tiempo);
            if (this.tiempo < 0)
            {
                this.Activo = false;
                PHoraDeLaRuleta.SetActive(this.Activo);
                PGanaste.SetActive(this.Activo);
                PPerdiste.SetActive(this.Activo);
                PCorrecto.SetActive(this.Activo);
                PIncorrecto.SetActive(this.Activo);
            }
        }
	}

    public void MuestraPanel(string panel)
    {
        if (this.Activo)
        {
            //Debug.Log("Ya estoy activo");
            return;
        }
        this.Activo = true;
        tiempo = 4f;
        switch (panel)
        {
            case "ruleta":
                PHoraDeLaRuleta.SetActive(this.Activo);
                break;
            case "ganaste":
                PGanaste.SetActive(this.Activo);
                break;
            case "perdiste":
                PPerdiste.SetActive(this.Activo);
                break;
            case "correcto":
                PCorrecto.SetActive(this.Activo);
                break;
            case "incorrecto":
                PIncorrecto.SetActive(this.Activo);
                break;
            default:
                break;
        }
    }
}
