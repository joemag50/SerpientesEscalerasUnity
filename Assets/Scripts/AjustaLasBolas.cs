﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Data;
using Mono.Data.SqliteClient;


public class AjustaLasBolas : MonoBehaviour
{
    public int Temas;
    public GameObject BolaOriginal;
    public bool BolasAjustadas;
    public GameObject LineaOriginal;

    private float radio;
    private float theta;
    private float ThetaScale;

    // Use this for initialization
    void Start ()
    {
        CantidadTemas();
        AjustamosNuestrasBolas();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CantidadTemas()
    {
        IDataReader reader = BaseDatos.SqlCommand("SELECT * FROM temas");
        while (reader.Read())
        {
            this.Temas++;
        }
    }

    public void AjustamosNuestrasBolas()
    {
        ThetaScale = (1f/ (float) this.Temas);
        radio = 35;
        theta = 0f;
        Vector3 ajuste = new Vector3(0,0,0);
        int Size = (int)((1f / ThetaScale) + 1f);

        GameObject ConjuntoDeLineas = new GameObject();
        ConjuntoDeLineas.transform.position = transform.position;
        for (int i = 0; i < Size; i++)
        {
            theta += (2.0f * Mathf.PI * ThetaScale);
            ajuste.x = radio * Mathf.Cos(theta);
            ajuste.y = radio * Mathf.Sin(theta);

            GameObject bola;
            bola = Instantiate(BolaOriginal, this.transform.position + ajuste, Quaternion.identity);
            bola.name = "Bola-" + i;
            bola.transform.SetParent(transform);
            bola.tag = "BolaPedorra";

            //Vamos a poner una lineas como si fueran los limites

            GameObject linea = Instantiate(LineaOriginal, Vector3.zero, Quaternion.identity);
            linea.GetComponent<LineRenderer>().SetPosition(1, this.transform.position + new Vector3(0, 0, -3));
            linea.GetComponent<LineRenderer>().SetPosition(0, this.transform.position + ajuste + new Vector3(0, 0, -3));
            linea.GetComponent<LineRenderer>().startWidth = 0.3f;
            linea.GetComponent<LineRenderer>().endWidth = 0.3f;
            linea.name = "LineaPedorra";
            linea.transform.SetParent(ConjuntoDeLineas.transform);
            linea.GetComponent<LineRenderer>().useWorldSpace = false;
        }
        ConjuntoDeLineas.name = "ConjuntoLineas";
        ConjuntoDeLineas.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 23));
        ConjuntoDeLineas.transform.SetParent(transform);
        BolasAjustadas = true;
    }
}
