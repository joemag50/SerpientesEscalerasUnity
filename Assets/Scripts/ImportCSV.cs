﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Data;
using Mono.Data.SqliteClient;

public class ImportCSV : MonoBehaviour
{
	// Use this for initialization
	void Start ()
    {
        //PreguntasFelipeToPreguntas();
        //PreguntasRomanToPreguntas();
        //PreguntasGerardoToPreguntas();
        //Debug.Log("Terminado");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PreguntasFelipeToPreguntas()
    {
        IDataReader reader = BaseDatos.SqlCommand("SELECT * FROM preguntasfelipe ORDER BY idpregunta ");

        while (reader.Read())
        {
            string idpregunta = reader.GetString(0);
            string idtema = reader.GetString(1);
            string idpregunta_tema = reader.GetString(2);
            string pregunta = reader.GetString(3);
            string aaa = reader.GetString(4);
            string bbb = reader.GetString(5);
            string ccc = reader.GetString(6);
            string ddd = reader.GetString(7);
            string correcta = reader.GetString(8);
            string[] parametros = {"idtema:"+idtema, "idpregunta_tema:"+idpregunta_tema, "pregunta:"+pregunta, "aaa:"+aaa, "bbb:"+bbb, "ccc:"+ccc, "ddd:"+ddd, "correcta:"+correcta};
            BaseDatos.SqlCommand(" INSERT INTO preguntas (idtema, idpregunta_tema, pregunta, aaa, bbb, ccc, ddd, correcta) VALUES " +
                                               " (@idtema, @idpregunta_tema, @pregunta, @aaa, @bbb, @ccc, @ddd, @correcta) ", parametros);
        }
    }

    public void PreguntasRomanToPreguntas()
    {
        IDataReader reader = BaseDatos.SqlCommand("SELECT * FROM roman ORDER BY idpregunta ");

        while (reader.Read())
        {
            string idpregunta = reader.GetString(0);
            string idtema = reader.GetString(1);
            string idpregunta_tema = reader.GetString(2);
            string pregunta = reader.GetString(3);
            string aaa = reader.GetString(4);
            string bbb = reader.GetString(5);
            string ccc = reader.GetString(6);
            string ddd = reader.GetString(7);
            string correcta = reader.GetString(8);
            string[] parametros = { "idtema:" + idtema, "idpregunta_tema:" + idpregunta_tema, "pregunta:" + pregunta, "aaa:" + aaa, "bbb:" + bbb, "ccc:" + ccc, "ddd:" + ddd, "correcta:" + correcta };
            BaseDatos.SqlCommand(" INSERT INTO preguntas (idtema, idpregunta_tema, pregunta, aaa, bbb, ccc, ddd, correcta) VALUES " +
                                               " (@idtema, @idpregunta_tema, @pregunta, @aaa, @bbb, @ccc, @ddd, @correcta) ", parametros);
        }
    }

    public void PreguntasGerardoToPreguntas()
    {
        IDataReader reader = BaseDatos.SqlCommand("SELECT * FROM roman ORDER BY idpregunta ");

        while (reader.Read())
        {
            string idpregunta = reader.GetString(0);
            string idtema = reader.GetString(1);
            string idpregunta_tema = reader.GetString(2);
            string pregunta = reader.GetString(3);
            string aaa = reader.GetString(4);
            string bbb = reader.GetString(5);
            string ccc = reader.GetString(6);
            string ddd = reader.GetString(7);
            string correcta = reader.GetString(8);
            string[] parametros = { "idtema:" + idtema, "idpregunta_tema:" + idpregunta_tema, "pregunta:" + pregunta, "aaa:" + aaa, "bbb:" + bbb, "ccc:" + ccc, "ddd:" + ddd, "correcta:" + correcta };
            BaseDatos.SqlCommand(" INSERT INTO preguntas (idtema, idpregunta_tema, pregunta, aaa, bbb, ccc, ddd, correcta) VALUES " +
                                               " (@idtema, @idpregunta_tema, @pregunta, @aaa, @bbb, @ccc, @ddd, @correcta) ", parametros);
        }
    }
}
