﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiarCamara : MonoBehaviour
{
	public static Camera CamTablero;
	public static Camera CamRuleta;
	public bool cam;
	public static int CualCamara;
	void Start()
	{
		CamTablero = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
		CamRuleta = GameObject.FindGameObjectWithTag("CamRuleta").GetComponent<Camera>();
		cam = true;
		CualCamara = -1;
	}

	void Update()
	{
		//if (CualCamara == 1)

		if (Input.GetKeyDown(KeyCode.Space) || CualCamara == 1)
		{
			if (cam)
			{
				CamRuletaActivar();
				cam = false;
			}
			else
			{
				CamTableroActivar();
				cam = true;
			}

			CualCamara = -1;
		}
	}

	public static void CamRuletaActivar()
	{
		CamTablero.enabled = false;
		CamRuleta.enabled = true;
	}

	public static void CamTableroActivar()
	{
		CamTablero.enabled = true;
		CamRuleta.enabled = false;
	}
}
