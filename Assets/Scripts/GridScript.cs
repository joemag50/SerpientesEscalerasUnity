﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GridScript : MonoBehaviour
{
    public GameObject mosaicoBlanco, mosaicoNegro, Jugador, Escalera, Serpiente;
    Material[] mats;
    List<Mosaico> mosaicos;

    public int resolution;
    public int WidHei;

    Vector3 Size;

    //Size.x = cols
    //Size.y = rows
    // Use this for initialization
    void Start ()
    {
        int wid = WidHei;
        int hei = WidHei;
        Size = new Vector3(wid/resolution, 0, hei/resolution);
        int x = 0;
        int z = 0;
        int dir = 1;

        mosaicos = new List<Mosaico>();

        for (int i = 0; i < (int) (Size.x * Size.z); i++)
        {
            if (i == 0)
            {
                //Luego ponemos un loop para agregar a todos
                Instantiate(Jugador, new Vector3(x + this.transform.position.x, 0, z + this.transform.position.z), Quaternion.identity);
            }
            GameObject mos;
            if (i % 2 == 0)
            {
                 mos = Instantiate(mosaicoBlanco, new Vector3(x + this.transform.position.x, 0, z + this.transform.position.z), Quaternion.Euler(90,0,0));
            }
            else
            {
                mos = Instantiate(mosaicoNegro, new Vector3(x + this.transform.position.x, 0, z + transform.position.z), Quaternion.Euler(90, 0, 0));
            }
            mos.name = "Mosaico" + i;
            mos.transform.SetParent(transform);
            mos.GetComponentInChildren<TextMeshProUGUI>().text = (i+1)+"";

            int snadder = 0;
            mosaicos.Add(new Mosaico(mos, i, snadder));

            x += (resolution * dir);
            if (x >= wid || x <= -resolution)
            {
                dir *= -1;
                x += resolution * dir;
                z += resolution;
            }

        }

        //Escaleras
        for (int i = 0; i < 3; i++)
        {
            //Significa que la primera y la ultima fila no cuentan
            int inicioEscalera = Random.Range(8, 42);
            int longitud = Random.Range(4,6);

            Vector3 mosIniCenter = mosaicos[inicioEscalera].GetCenter();
            Vector3 mosFinCenter = mosaicos[inicioEscalera+longitud].GetCenter();

            mosaicos[inicioEscalera].SetSnadder(longitud);

            GameObject escalera = Instantiate(Escalera, Vector3.zero, Quaternion.identity);
            escalera.GetComponent<LineRenderer>().SetPosition(0, mosIniCenter);
            escalera.GetComponent<LineRenderer>().SetPosition(1, mosFinCenter);
        }

        //Serpientucas
        for (int i = 0; i < 3; i++)
        {
            //Significa que la primera y la ultima fila no cuentan
            int inicioEscalera = Random.Range(8, 42);
            int longitud = Random.Range(4, 6);

            Vector3 mosIniCenter = mosaicos[inicioEscalera].GetCenter();
            Vector3 mosFinCenter = mosaicos[inicioEscalera - longitud].GetCenter();

            mosaicos[inicioEscalera].SetSnadder(-longitud);

            GameObject serpiente = Instantiate(Serpiente, Vector3.zero, Quaternion.identity);
            serpiente.GetComponent<LineRenderer>().SetPosition(1, mosIniCenter);
            serpiente.GetComponent<LineRenderer>().SetPosition(0, mosFinCenter);
        }

        GameObject.FindGameObjectWithTag("Player").GetComponent<Jugador>().misMosaicos(mosaicos);
	}
}
