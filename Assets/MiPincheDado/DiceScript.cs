﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceScript : MonoBehaviour
{
    static Rigidbody rb;
    public static Vector3 diceVelocity;
    public static bool rolling;
    public static int diceNumber;

    public float fuerza;
    public bool inicio;

    // Use this for initialization
    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        rolling = false;
        inicio = true;
	}
	
	// Update is called once per frame
	void Update ()
    {
        diceVelocity = rb.velocity;

        if (diceVelocity != Vector3.zero)
            rolling = true;
        else
            rolling = false;

        if (inicio)
        {
            transform.position = new Vector3(transform.position.x, 40, transform.position.z);
            rolling = true;
        }
    }

    public void GirarDadoChingon()
    {
        inicio = false;
        diceVelocity = rb.velocity;

        diceNumber = 0;
        float dirX = Random.Range((fuerza * 3) / 4, fuerza);
        float dirY = Random.Range((fuerza * 3) / 4, fuerza);
        float dirZ = Random.Range((fuerza * 3) / 4, fuerza);

        transform.position = new Vector3(transform.position.x, 40, transform.position.z);
        transform.rotation = transform.rotation;
        rb.AddForce(transform.up * fuerza);
        rb.AddTorque(dirX, dirY, dirZ);
    }
}
